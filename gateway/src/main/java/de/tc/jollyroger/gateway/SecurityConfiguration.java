package de.tc.jollyroger.gateway;

import org.springframework.context.annotation.Bean;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.security.config.annotation.web.reactive.EnableWebFluxSecurity;
import org.springframework.security.config.web.server.ServerHttpSecurity;
import org.springframework.security.oauth2.client.userinfo.CustomUserTypesOAuth2UserService;
import org.springframework.security.oauth2.client.userinfo.OAuth2UserRequest;
import org.springframework.security.oauth2.client.userinfo.ReactiveOAuth2UserService;
import org.springframework.security.web.server.SecurityWebFilterChain;
import reactor.core.publisher.Mono;

import java.util.Map;

@EnableWebFluxSecurity
public class SecurityConfiguration {

	@Bean
	@Order(Ordered.HIGHEST_PRECEDENCE)
	ReactiveOAuth2UserService oAuth2UserService() {
		CustomUserTypesOAuth2UserService customUserTypesOAuth2UserService = new CustomUserTypesOAuth2UserService(
				Map.of("nextcloud", NextcloudUser.class));
		return (ReactiveOAuth2UserService<OAuth2UserRequest, NextcloudUser>) userRequest -> Mono
				.just((NextcloudUser) customUserTypesOAuth2UserService
						.loadUser(userRequest));
	}

	@Bean
	SecurityWebFilterChain springSecurityFilterChain(ServerHttpSecurity http) {
		return http
				// .csrf().disable()//
				.authorizeExchange()//
				.pathMatchers("/api/**/public/**").permitAll()//
				.pathMatchers("/api/**/admin/**").hasRole("ADMIN")//
				.pathMatchers("/api/**").authenticated()//
				.anyExchange().permitAll()//
				.and().oauth2Login()//
				.and().build();
	}

}
