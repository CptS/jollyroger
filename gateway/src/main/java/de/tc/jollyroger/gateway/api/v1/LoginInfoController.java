package de.tc.jollyroger.gateway.api.v1;

import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Validated
public class LoginInfoController {

	@GetMapping("/api/v1/user")
	public ResponseEntity<OAuth2User> user(@AuthenticationPrincipal OAuth2User oauth2User) {
		return ResponseEntity.ok(oauth2User);
	}

}
