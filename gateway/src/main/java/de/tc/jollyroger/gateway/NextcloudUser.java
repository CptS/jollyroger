package de.tc.jollyroger.gateway;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.oauth2.core.user.OAuth2User;

import java.io.Serializable;
import java.time.OffsetDateTime;
import java.util.*;
import java.util.stream.Collectors;

import static org.springframework.util.StringUtils.hasText;

@Data
public class NextcloudUser implements OAuth2User, Serializable {

	private Ocs ocs;

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return Optional.ofNullable(ocs).map(Ocs::getData).map(OcsData::getGroups)
				.map(groups -> groups.stream()
						.map(group -> new SimpleGrantedAuthority(
								"ROLE_" + group.toUpperCase()))
						.collect(Collectors.toList()))
				.orElseGet(() -> Collections
						.singletonList(new SimpleGrantedAuthority("ROLE_ANONYMOUS")));
	}

	@Override
	public Map<String, Object> getAttributes() {
		return null;
	}

	@Override
	public String getName() {
		return Optional.ofNullable(ocs).map(Ocs::getData)
				.map(ocsData -> hasText(ocsData.getDisplayName())
						? ocsData.getDisplayName() : ocsData.getId())
				.orElse("Anonym");
	}

	@Data
	private static class Ocs implements Serializable {

		private OcsMeta meta;

		@Getter
		private OcsData data;

	}

	@Data
	private static class OcsMeta implements Serializable {

		private String status;

		private int statuscode;

		private String message;

	}

	@Data
	private static class OcsData implements Serializable {

		private boolean enabled;

		private String id;

		private OffsetDateTime lastLogin;

		private String email;

		private String phone;

		private String address;

		private String website;

		private String twitter;

		private List<String> groups;

		private String language;

		@JsonProperty("display-name")
		private String displayName;

	}

}
