package de.tc.jollyroger.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.core.user.OAuth2User;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.Optional;

@SpringBootApplication
@Controller
public class JollyrogerGatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(JollyrogerGatewayApplication.class, args);
	}

	@GetMapping("/")
	public String globalHomepage(@AuthenticationPrincipal OAuth2User oauth2User,
			Model model) {
		model.addAttribute("loggedIn", oauth2User != null);
		model.addAttribute("username",
				Optional.ofNullable(oauth2User).map(OAuth2User::getName).orElse("Gast"));
		return "index";
	}

	@GetMapping("/error")
	public String error() {
		return "error";
	}

}
