package de.tc.jollyroger.budget.model;

import lombok.Data;

import java.util.List;

@Data
public class User {

	private String id;

	private String displayName;

	private List<String> roles;

	private List<Group> groups;

}
