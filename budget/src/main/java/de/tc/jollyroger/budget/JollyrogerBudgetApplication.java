package de.tc.jollyroger.budget;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;

@SpringBootApplication
public class JollyrogerBudgetApplication {

	public static void main(String[] args) {
		SpringApplication.run(JollyrogerBudgetApplication.class, args);
	}

	@GetMapping("/")
	public String index() {
		return "index";
	}

}
