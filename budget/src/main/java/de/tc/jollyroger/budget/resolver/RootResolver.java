package de.tc.jollyroger.budget.resolver;

import com.coxautodev.graphql.tools.GraphQLQueryResolver;
import de.tc.jollyroger.budget.model.User;
import graphql.schema.DataFetchingEnvironment;
import org.springframework.stereotype.Component;

@Component
public class RootResolver implements GraphQLQueryResolver {

	public User user(DataFetchingEnvironment env) {
		return new User();
	}

}
