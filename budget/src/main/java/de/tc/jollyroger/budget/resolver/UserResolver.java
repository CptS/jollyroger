package de.tc.jollyroger.budget.resolver;

import com.coxautodev.graphql.tools.GraphQLResolver;
import de.tc.jollyroger.budget.model.User;
import org.springframework.stereotype.Component;

@Component
public class UserResolver implements GraphQLResolver<User> {

}
