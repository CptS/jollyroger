package de.tc.jollyroger.budget.model;

import lombok.Data;

@Data
public class Group {

	private String id;

	private String displayName;

}
