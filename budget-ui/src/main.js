import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import VueRouter from 'vue-router'
import HomePage from './pages/HomePage'
import InsertPage from "./pages/InsertPage";
import NotFoundPage from "./pages/error/NotFoundPage";

Vue.use(VueRouter);

const router = new VueRouter({
    mode: 'history',
    routes: [
        {path: '/', component: HomePage},
        {path: '/insert', component: InsertPage},
        {path: '*', component: NotFoundPage},
    ],
});

new Vue({
    router,
    render: h => h(App),
}).$mount('#app');
