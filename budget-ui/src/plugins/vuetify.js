import Vue from 'vue'
import Vuetify from 'vuetify/lib'
import './theme.styl'
import 'vuetify/src/stylus/app.styl'
import 'material-design-icons-iconfont/dist/material-design-icons.css'

Vue.use(Vuetify, {
    iconfont: 'md',
    theme: {
        primary: '#827717',
        accent: '#006064',
    },
});
